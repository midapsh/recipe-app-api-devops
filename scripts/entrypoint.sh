#!/bin/sh

set -e

# Collect static:
## Allows you to collect all
## of the static files required
## for your project and store
## them in a single directory
python manage.py collectstatic --noinput

# Database migrations
## Migration file to make necessary
## changes to database
## Adding / removing a table
python manage.py wait_for_db
python manage.py migrate

# 'uwsgi': name of the application
# '--socket :9000': Run uWSGI as a TCP socket on port 9000
# '--workers 4': number of threads
# '--master': run this as the master service on the terminal
# '--enable-threads': enable threads
# '--module app.wsgi': actual application that uWSGI is going to run
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

