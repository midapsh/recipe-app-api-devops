variable "prefix" {
  // Project abbreviation
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-deveops"
}

variable "contact" {
  default = "henrique@spadim.com.br"
}