terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-at-hspadim"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

// Locals are ways that you can create dynamic variable inside Terraform
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}

// Public subnets
data "aws_region" "current" {}
