data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name = "name"
    // Get AMI name from AWS
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  // 'data.': retrieve info from AWS
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}